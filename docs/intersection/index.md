# Intersection

Dans cette section, nous allons voir comment nous pouvons calculer l'intersection de deux éléments. Les cas d'usages
peuvent être nombreux :

- Vérifier si un point est dans une surface
- Calculer l'intersection entre deux surfaces
- ...