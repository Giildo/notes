# Intersection de deux cercles

## Rappels de base

Pour savoir si deux cercles se touchent ou s'intersectent l'un l'autre, il faut calculer la distance entre leurs centres
et vérifier si cette distance est inférieure à la somme des rayons. Si c'est le cas, c'est qu'il y a intersection.

Pour calculer la distance entre deux cercles, on peut utiliser le théorème de Pythagore.

![Intersection de deux cercles](/circles-intersection.webp)

La distance entre les centres des deux cercles est :
$$ BC = \sqrt{ AB^2 + AC^2 } $$
... et si ...

$$ BC < radiusC1 + radiusC2 $$

... c'est qu'il y a intersection.

## En JS

Les coordonnées des centres des cercles sont $x1$, $y1$, $x2$, $y2$.

```js
const radiusC1 = 4
const radiusC2 = 4
const AC = x1 - x2
const AB = y1 - y2
const BC = Math.sqrt(Math.pow(AC) + Math.pow(AB))
const hasIntersection = BC < radiusC1 + radiusC2
```

