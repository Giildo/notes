# Trigonométrie

Utilisation générale liée à la trigonométrie.

La trigonométrie est utilisée dans de nombreux domaines :

- Dessin vectoriel
- Animations
- Logique de placement