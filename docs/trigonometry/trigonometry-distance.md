# La trigonométrie et les distances

## Trouver les coordonnées d'un point

À partir d'un angle en radian, on peut retrouver les coordonnées d'un point via les `cosinus` et `sinus`.

```js
const angle = Math.PI / 4
const radius = 5
const x = Math.cos(angle) * radius
const y = Math.sin(angle) * radius
```

![Cercle trigonométrique](/illustration_trigonometric_circle_sin_cos.webp)

## Trouver l'angle d'un point

À l'inverse, si on connaît les coordonnées d'un point, on peut retrouver l'angle via sa `tangente`.

```js
const angle = Math.atan2(y, x)
```

## Inverser un des élements d'un angle

Lorsque j'ai codé un jeu de balle, j'ai dû inverser les angles lorsque la balle entrait en collision avec un mur. Pour
se faire, on devait trouver l'inverse soit du `cosinus`, soit du `sinus`.

Par exemple, si la balle allait vers le bas et vers la droite, qu'elle entrait en collision avec le mur de droite, on
devait la faire repartir vers le bas et vers la gauche. On passait alors d'un angle de `-π/4` vers un angle de `-3π/4`.

```js
const angle = -Math.PI / 4
const newAngle = Math.atan2(Math.sin(angle), -Math.cos(angle))
```
