# Rappels de base

## Le cercle trigonométrique

Un cercle trigonométrique est une figure géométrique, ayant un rayon d'une unité (m, cm, dm, mm...). Dans ce cercle
passent les axes d'un graphique : axe des abscisses (axe des x) et axe des ordonnées (axe des y).

::: tip
Comme l'unité n'est pas définie, cela permet de toujours avoir, quel que soit l'usage qu'on a de ce cercle, une valeur
de x ou y comprise entre 0 et 1. Si, par exemple, je travaille sur un cercle de rayon 360px, j'aurai une valeur comprise
entre 0 et 360px.
:::

![Cercle trigonométrique](/illustration_trigonometric_circle.webp)

