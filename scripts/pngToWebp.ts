import { argv, exit } from 'node:process'
import { stat } from 'node:fs/promises'
import sharp from 'sharp'

const namedArgs: Record<string, string> = {}
for (let i = 0; i < argv.length; i++) {
  const arg = argv[i]
  if (arg.startsWith('--')) {
    const equalArg = arg.split('=')

    if (equalArg.length === 2) {
      namedArgs[equalArg[0].slice(2)] = equalArg[1]
      continue
    }

    namedArgs[arg.slice(2)] = argv[i + 1]
    i++
  }
}

if (!('file' in namedArgs)) {
  console.error('Vous devez fournir un fichier')
  exit(1)
}

if (!('output' in namedArgs)) {
  console.error('Vous devez fournir le dossier de sortie')
  exit(1)
}

if (!('name' in namedArgs)) {
  console.error('Vous devez fournir un nom pour le fichier')
  exit(1)
}

const file = await stat(namedArgs.file)
if (!file.isFile()) {
  console.error('Not a file')
  exit(1)
}

try {
  if (!['jpg', 'jpeg', 'png'].includes(namedArgs.file.split('.').pop()!)) {
    console.error('Le format du fichier doit être jpg, jpeg ou png')
    exit(1)
  }

  await sharp(namedArgs.file).webp().toFile(namedArgs.output + '/' + namedArgs.name + '.webp')
} catch (e) {
  console.error(e)
  exit(1)
}


console.log('ok')